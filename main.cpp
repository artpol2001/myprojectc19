#include <string>
#include <iostream>

using namespace std;

class Animal
{
protected:
    string name;
public:
    
    Animal(string name) : name(name)
    {}

    virtual void Voice() = 0;
    virtual ~Animal() { "Animals"; }
};

class Dog : public Animal
{
public:
    Dog(string name) : Animal(name)
    {
    }

    void Voice() override
    {
        cout << name << " says woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    Cat(string name) : Animal(name)
    {
    }

    void Voice() override
    {
        cout << name << " says mew-mew" << endl;
    }
};

class goat : public Animal
{
public:
    goat(string name) : Animal(name)
    {
    }

    void Voice() override
    {
        cout << name << " says meeeeee" << endl;
    }
};

int main()
{
     Animal* animals[] = { new Dog("Dog"), new Cat("Cat"),new goat("goat") };
     animals[0]->Voice();
     animals[1]->Voice();
     animals[2]->Voice();
    delete animals[0]; delete animals[1]; delete animals[2];
}